/*
 *
 *  *     Copyright 2020 Nikos Petalidis @ https://www.petalidis.gr
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *          http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *
 */

package gr.petalidis.frontend.service;

import com.vaadin.flow.server.VaadinSession;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestClientException;

import java.io.Serializable;
import java.util.List;

@Service
public class CustomerListService implements Serializable {
    private final static org.slf4j.Logger logger = LoggerFactory.getLogger(CustomerListService.class.getName());

    private final KeycloakRestTemplate keycloakRestTemplate;

    @Autowired
    public CustomerListService(KeycloakRestTemplate keycloakRestTemplate)
    {
        this.keycloakRestTemplate = keycloakRestTemplate;
    }

    public HttpHeaders buildHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", "application/json");
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Authorization", "Bearer " + VaadinSession.getCurrent().getAttribute("token"));
        return headers;
    }
    // WebClient is the preferred method now in in Spring Boot

    //   public static WebClient buildHttpHeaders(String token) {

//        return WebClient.builder()
//                .baseUrl("http://localhost:8090")
//                .defaultHeader("Content-type", "application/json")
//                .defaultHeader("Authorization","Bearer " + token).build();
 //   }

    private void checkStatus(ResponseEntity responseEntity)  {
        HttpStatus status = responseEntity.getStatusCode();
        if (status != HttpStatus.OK) {
            String body = (String) responseEntity.getBody();
            logger.error("Error response with status Code {} and body {}", status, body);
            throw new RestClientException(body);
        }
    }
    public  List<String> getCustomers() {

        ResponseEntity<List<String>> response = keycloakRestTemplate.exchange("http://localhost:8081/customers",
                HttpMethod.GET,
                new HttpEntity<>(buildHttpHeaders()),
                new ParameterizedTypeReference<List<String>>() {
                });

        checkStatus(response);
        return response.getBody();
//        List<String> response = buildHttpHeaders("")
//                .post()
//                .uri(URI.create("/customers"))
//                .retrieve()
//                .bodyToMono(new ParameterizedTypeReference<List<String>>() {
//                }).block();
//
//        return response;
    }

}
